import express  from "express";
import  {registerController , loginController , testController, forgotPasswordController} from "../controllers/authController.js"
import { isAdmin, requireSignIn } from "../middleware/authMiddleware.js";
// create router object 

const router = express.Router()

// routing 


// REGISTER  || METHOD POST
router.post("/register", registerController);


//  LOGIN || POST
router.post("/login" , loginController)

//forgot password
router.post('/forgot-password', forgotPasswordController)

// test routes
router.get('/test',requireSignIn,isAdmin, testController)



// protected  USER  route auth
//requiresignin is built in function   for USER

router.get("/user-auth",requireSignIn,(req,res)=>{
    res.status(200).send({ok:true});
})

// protected  Admin  route auth
//requiresignin is built in function  for ADMINNN

router.get("/admin-auth",requireSignIn, isAdmin,(req,res)=>{
    res.status(200).send({ok:true});
})





export default router;
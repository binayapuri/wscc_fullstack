import { Route, Routes } from 'react-router-dom';
import './App.css';
import About from './pages/About';
import Register from './pages/Auth/Register';
import Contact from './pages/Contact';
import HomePage from './pages/HomePage';
import Pagenotfound from './pages/Pagenotfound';
import Policy from './pages/Policy';
// import { ToastContainer,toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Login from './pages/Auth/Login';
import Dashboard from './pages/user/Dashboard';
import PrivateRoute from './components/Routes/Private';
import ForgotPassword from './pages/Auth/ForgotPassword';
import AdminRoute from './components/Routes/AdminRoute';
import AdminDashboard from './pages/Admin/AdminDashboard';
import CreateCategory from './pages/Admin/CreateCategory';
import CreateProduct from './pages/Admin/CreateProduct';
import Users from './pages/Admin/Users';
import Orders from './pages/user/Orders';
import Profile from './pages/user/Profile';


function App() {
  return (
    <>
    <Routes>
      <Route path='/' element={<HomePage/>} />


      {/* nested route for user protected */}
      <Route path="/dashboard" element={<PrivateRoute/>}>
      <Route path='user' element={<Dashboard></Dashboard>} />
      <Route path='user/orders' element={<Orders/>} />
      <Route path='user/profile' element={<Profile/>} />

      </Route>

      {/* routes for ADMIN */}

      <Route path="/dashboard" element={<AdminRoute/>}>
      <Route path='admin' element={<AdminDashboard/>} />
      <Route path='admin/create-category' element={<CreateCategory/>} />
      <Route path='admin/create-product' element={<CreateProduct/>} />
      <Route path='admin/users' element={<Users/>} />

      </Route>


      <Route path='/register' element={<Register/>} />
      <Route path='/forgot-password' element={<ForgotPassword/>} />
      <Route path='/about' element={<About/>} />
      <Route path='/policy' element={<Policy/>} />
      <Route path='/contact' element={<Contact/>} />
      <Route path='/login' element={<Login/>} />


      <Route path='*' element={<Pagenotfound/>} />
    </Routes>
    </>
  );
}

export default App;

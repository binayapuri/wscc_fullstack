import React from 'react'
import { Link } from 'react-router-dom'
import Layout from '../components/layouts/Layout'

const Pagenotfound = () => {
  return (
    <Layout title={"Error 404"}>
    <div className="container py-5 m-2">
        <div className="row justify-content-center align-items-center">
          <div className="col-md-6 text-center">
            <h1 className="display-4 mb-4">Oops! Page not found</h1>
            <p className="lead">The page you are looking for could not be found.</p>
            <Link to="/" className="btn btn-primary m-3">Go back to home</Link>
          </div>
          <div className="col-md-6 text-center">
            <img src="https://picsum.photos/500/501" alt="404 error" className="img-fluid" />
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default Pagenotfound

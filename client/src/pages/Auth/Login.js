import React, { useState } from 'react'
import '../../styles/RegStyle.css'
import axios from 'axios'
import {useNavigate ,  useLocation} from 'react-router-dom'
import Layout from '../../components/layouts/Layout'
import toast from 'react-hot-toast';
import { useAuth } from '../../context_api/auth'
const Login = () => {

    const [auth,setAuth] = useAuth();

    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
 
    const navigate = useNavigate()
    const location = useLocation()

        //form function for login
        const handleSubmit = async (e) =>{
            e.preventDefault(); 
            try{
                const res = await axios.post(`/api/v1/auth/login`,{password,email})
                if(res && res.data.success){
                    toast.success (res && res.data.message);
                    // success vaya paxi navigate vanda agadi user ko data aauxa
                    setAuth({
                        
                        //
                        ...auth,
                        user:res.data.user,
                        token:res.data.token,
                    })
                    //localstorage ma save garinxa
                    localStorage.setItem('auth',JSON.stringify(res.data))
                    navigate(location.state||'/');
                }
                else{
                    toast.error(res.data.message)
                    
                }
            }catch(err){
                console.log(err)
            }
        }
  return (
    <div>
              <Layout title={"Login"}>
            <div className='form-container'>
                
                <form onSubmit={handleSubmit}>
                <h4 className='text-center text-primary'>Login</h4>

                    <div className="mb-3 mt-3">
                        <input type="email"
                            value={email}
                            onChange={(e)=>setEmail(e.target.value)}
                            className="form-control"
                            id="regemail"
                            aria-describedby="emailHelp"
                            placeholder='Enter your email'
                            required
                            />
                    </div>

                    <div className="mb-3">
                        <input type="password"
                            value={password}
                            onChange={(e)=>setPassword(e.target.value)}
                            className="form-control" id="InputPassword1" placeholder='Enter your Password'
                            required
                            />
                    </div>
                    <div className='mb-3'>
                    <button type="button" className="btn btn-primary" onClick={()=>{
                        navigate('/forgot-password')
                    }} >Forgot Password</button>

                    </div>
                    <div>
                    <button type="submit" className="btn btn-primary">Login</button>

                    </div>


                </form>



            </div>


        </Layout>
    </div>
  )
}

export default Login

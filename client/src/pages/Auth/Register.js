import React, { useState } from 'react'
import '../../styles/RegStyle.css'
import axios from 'axios'
import {useNavigate} from 'react-router-dom'
import Layout from '../../components/layouts/Layout'
import {toast} from 'react-toastify';



  
const Register = () => {
    const [name, setName] = useState("")
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [address, setAddress] = useState("")
    const [question, setQuestion] = useState("")

    const [phone, setPhone] = useState("")
    const navigate = useNavigate()


    //form function 
    const handleSubmit = async (e) =>{
        e.preventDefault(); 
        try{
            const res = await axios.post(`/api/v1/auth/register`,{name,password,email,address,question,phone})
            if(res && res.data.success){
                toast.success(res.data && res.data.message)

                navigate('/login');

            }
            else{
                toast.error(res.data.message)
                
            }
        }catch(err){
            console.log(err)
        }
    }

    

    return (

        <Layout title={"Register"}>
            <div className='form-container'>
                
                <form onSubmit={handleSubmit}>
                <h4 className='text-center text-primary'>Register Page</h4>
                    <div className="mb-3 mt-3">
                        <input type="text"
                            value={name}
                            onChange={(e) => setName(e.target.value)}
                            className="form-control"
                            id="regname"
                            aria-describedby="emailHelp"
                            placeholder='Enter your name'
                            required />
                           
                    </div>
                    <div className="mb-3 mt-3">
                        <input type="email"
                            value={email}
                            onChange={(e)=>setEmail(e.target.value)}
                            className="form-control"
                            id="regemail"
                            aria-describedby="emailHelp"
                            placeholder='Enter your email'
                            required
                            />
                    </div>
                    <div className="mb-3 mt-3">
                        <input type="text"
                            value={question}
                            onChange={(e) => setQuestion(e.target.value)}
                            className="form-control"
                            id="regname"
                            aria-describedby="emailHelp"
                            placeholder='what is your favourate sports?'
                            required />
                           
                    </div>

                    <div className="mb-3">
                        <input type="password"
                            value={password}
                            onChange={(e)=>setPassword(e.target.value)}
                            className="form-control" id="InputPassword1" placeholder='Enter your Password'
                            required
                            />
                    </div>

                    <div className="mb-3">
                        <input type="number"
                            value={phone}
                            onChange={(e)=>setPhone(e.target.value)}
                            className="form-control"
                            id="regnum"
                            placeholder='Enter your Phone'
                            required
                           />
                    </div>


                    <div className="mb-3 mt-3">
                        <input type="text"
                            value={address}
                            onChange={(e)=>setAddress(e.target.value)  }
                            className="form-control"
                            id="regaddress"
                            aria-describedby="emailHelp"
                            placeholder='Enter your address'
                            required
                            
                            />
                    </div>

                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>



            </div>


        </Layout>

    )
}

export default Register

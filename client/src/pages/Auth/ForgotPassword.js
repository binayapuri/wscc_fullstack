import React, { useState } from 'react'
import '../../styles/RegStyle.css'
import axios from 'axios'
import {useNavigate} from 'react-router-dom'
import Layout from '../../components/layouts/Layout'
import toast from 'react-hot-toast';

const ForgotPassword = () => {

    const [email, setEmail] = useState("")
    const [newPassword, setNewPassword] = useState("")
    const [question, setQuestion] = useState("")

 
    const navigate = useNavigate()

        //form function for login
        const handleSubmit = async (e) =>{
            e.preventDefault(); 
            try{
                const res = await axios.post(`/api/v1/auth/forgot-password`,{newPassword,email,question})
                if(res && res.data.success){
                    
                    toast.success (res && res.data.message);
                    // success vaya paxi navigate vanda agadi user ko data aauxa

                    navigate('/login');
                }
                else{
                    toast.error(res.data.message)
                    
                }
            }catch(err){
                console.log(err)
            }
        }
    return (
        <Layout title={'forgot password - Ecommerce app'} >
            <div className='form-container'>

                <form onSubmit={handleSubmit}>
                    <h4 className='text-center text-primary'>Reset Password</h4>

                    <div className="mb-3 mt-3">
                        <input type="email"
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                            className="form-control"
                            id="regemail"
                            aria-describedby="emailHelp"
                            placeholder='Enter your email'
                            required
                        />
                    </div>

                    <div className="mb-3 mt-3">
                        <input type="text"
                            value={question}
                            onChange={(e) => setQuestion(e.target.value)}
                            className="form-control"
                            id="regemail"
                            aria-describedby="emailHelp"
                            placeholder='what is your favourate sports?'
                            required
                        />
                    </div>

                    <div className="mb-3">
                        <input type="password"
                            value={newPassword}
                            onChange={(e) => setNewPassword(e.target.value)}
                            className="form-control" id="InputPassword1" placeholder='Enter your Password'
                            required
                        />
                    </div>
                    <div>
                        <button type="submit" className="btn btn-primary">Reset</button>

                    </div>


                </form>



            </div>
        </Layout>
    )
}

export default ForgotPassword

import React from 'react'
import { Link } from 'react-router-dom'
import Layout from '../components/layouts/Layout'
import { AiFillFacebook, AiOutlineTwitter, AiFillInstagram } from 'react-icons/ai'

const Contact = () => {
  return (
    <Layout title={"Contact Us"}>
      <div className="contactus container py-5">
        <div className="row justify-content-center">
          <div className="col-md-8 col-lg-6 text-center ">
            <h1 className="display-4 mb-4">Contact Us</h1>
            <p className="lead mb-4">We'd love to hear from you!</p>
            <form className='container'>
              <div className="mb-3">
                <input type="text" placeholder='Name' className="form-control" id="name" />
              </div>
              <div className="mb-3">
                <input type="email" placeholder='Enter your email' className="form-control" id="email" />
              </div>
              <div className="mb-3">
                <textarea className="form-control" placeholder='Your Message!!!' id="message" rows="5"></textarea>
              </div>
              <button type="submit" className="btn btn-primary mb-3">Send Message</button>
            </form>
            <p className="mb-0">Or reach out to us on:</p>
            <div className="d-flex justify-content-center">
              <Link href="#" className="social-icon me-3"><i className="bi bi-twitter"><AiOutlineTwitter></AiOutlineTwitter></i></Link>
              <Link to="https://www.facebook.com/wesellcottoncandy" className="social-icon me-3"><i className=" bi bi-facebook"><AiFillFacebook></AiFillFacebook></i></Link>
              <Link to="https://www.instagram.com/wesellcottoncandy/" className="social-icon me-3"><i className="bi bi-instagram"><AiFillInstagram></AiFillInstagram></i></Link>
              <Link href="#" className="social-icon"><i className="bi bi-linkedin"></i></Link>
            </div>
          </div>
          <div className="col-md-4 col-lg-6 mt-5 mt-md-3">
            <img src="https://source.unsplash.com/random/660x601" className="img-fluid rounded align-content-center " alt="Contact Us" />
          </div>
        </div>
      </div>ƒ
    </Layout>
  )
}

export default Contact

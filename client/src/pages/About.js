import React from 'react'
import Layout from '../components/layouts/Layout'

const About = () => {
  return (
  <Layout title={"About"}>
      <div className="container my-5">
        <div className="row">
          <div className="col-md-6">
            <img src="https://picsum.photos/600/400" alt="Team" className="img-fluid" />
          </div>
          <div className="col-md-6">
            <h2>About Us</h2>
            <p className="lead">Lorem ipsum doloria blandit. Nam sodales est in magna eleifend, ut fermentum elit consequat. Duis a nulla at lacus consectetur rhoncus.</p>
            <p>Nulla facipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Integer dignissim, odio ut posuere imperdiet, enim ipsum consequat orci, nec dapibus augue enim non tellus. Nullam a malesuada orci. Vestibulum ac eros ut massa porttitor maximus ac ut arcu.</p>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default About

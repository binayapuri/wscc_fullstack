import React from 'react'
import Layout from '../components/layouts/Layout'
import {useAuth} from "../context_api/auth"

const HomePage = () => {
  const [auth,setAuth]=useAuth()
  return (
    <Layout title={"Home E-commerce"}>
    <h1>Home Page</h1>
    <pre>{JSON.stringify(auth,null,4)}</pre>
    </Layout>
  )
}

export default HomePage

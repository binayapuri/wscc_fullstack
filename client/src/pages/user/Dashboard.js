import React from 'react'
import Layout from '../../components/layouts/Layout'
import UserMenu from '../../components/layouts/UserMenu'
import { useAuth } from '../../context_api/auth'
const Dashboard = () => {
  const [auth] = useAuth()
  return (
    <Layout title={"Dashboard -Ecommerce"}>

      <div className="container-fluid m-3 p-3">
        <div className="row">
          <div className="col-md-3">
            <UserMenu />
          </div>
          <div className="col-md-9">Content
            <div className="card w-100 p-3">
              <h3>{auth?.user?.name}</h3>
              <h3>{auth?.user?.email}</h3>
              <h3>{auth?.user?.address}</h3>
            </div>
          </div>

        </div>


      </div>


    </Layout>
  )
}

export default Dashboard

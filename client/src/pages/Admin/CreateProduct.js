import React from 'react'
import AdminMenu from '../../components/layouts/AdminMenu'
import Layout from '../../components/layouts/Layout'

const CreateProduct = () => {
  return (
    <Layout title={"Dashboard - Create Products"} >
      <div className="container-fluid m-3 p-3 ">

        <div className="row">
          <div className="row col-md-3">
            <AdminMenu></AdminMenu>
          </div>
          <div className="row col-md-9">
            <h1>Create Products</h1>
          </div>

        </div>
      </div>

    </Layout>
  )
}

export default CreateProduct

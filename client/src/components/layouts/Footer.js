import React from 'react'
import { Link } from 'react-router-dom'

const Footer = () => {
  return (
    <div className='footer'>
      <h2 className='text-center'>All Right Reserved &copy; Binaya Puri</h2>
      <p className="text-center m-3">
        <Link to="/about">About</Link>
        |
        <Link to="/contact">Contact</Link>
        |
        <Link to="/Policy">Privacy Policy</Link>
      |
        <Link to="/about">Login</Link>



      </p>
    </div>
  )
}

export default Footer

import React from 'react'
import Footer from './Footer'
import Header from './Header'
import  { ToastContainer } from 'react-toastify';
import { Helmet } from "react-helmet";



const Layout = (props, description, keywords,author) => {
  return (
    <div>
      <Helmet>
        <meta charSet="utf-8" />
        <meta name="description" content={description} />
        <meta name="keywords" content={keywords} />
        <meta name="author" content={author} />
        <title>{props.title}</title>
      </Helmet>
      <Header></Header>
      <ToastContainer/>
      <main style={{ minHeight: "70vh" }}>
        {props.children}

      </main>
      <Footer />
    </div>
  )
}
// SEO friendly 
Layout.defaultProps ={
  title:"E-commerce app -Shop now",
  description: "e commerce project",
  keywords: "mern,react, node, mongodb",
  author:"Binaya puri",
}

export default Layout

import React from 'react'
import { useState, useEffect } from 'react';
import { LoaderIcon } from 'react-hot-toast';
import { useNavigate, useLocation } from 'react-router-dom';
const Spinner = ({path = "login"}) => {
    const [count, setCount] = useState(3)
    const navigate = useNavigate()
    const location = useLocation()

    useEffect(() => {
        const interval = setInterval(() => {
            setCount((prevValue) => --prevValue)
        }, 1000)
        count === 0 && navigate(`${path}`, {
            state: location.pathname
        }

        )


        return () => {
            clearInterval(interval)
        }
    }, [count, navigate, Location, path])

    return (
        <>
            <div class="d-flex flex-column align-items-center justify-content-center" style={{ height: "70vh" }}>
                <h1 className='text-center'>LOGIN FIRST TO ACCESS THIS</h1>

                <h1 className='text-center'>redirecting to you in {count} second</h1>
                <div class="spinner-border" role="status">
                    <span class="visually-hidden">Loading...</span>
                </div>
            </div>
        </>
    )
}

export default Spinner;
